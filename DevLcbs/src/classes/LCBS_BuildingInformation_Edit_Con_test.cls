@isTest(SeeAllData=true)
public class LCBS_BuildingInformation_Edit_Con_test
{  static Contact contact;
    static Building__c build;
    
    static testMethod void LCBS_BuildingInformation_Edit_Con_test(){
    
        build = LCBS_TestDataFactory.createBuilding(1)[0];
        contact=LCBS_TestDataFactory.createContact(3)[0];
        
        PageReference pageRef = Page.LCBS_BuildingInformation_Edit;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('id', build.id);
                 
        LCBS_BuildingInformation_Edit_Controller bc = new LCBS_BuildingInformation_Edit_Controller();
        bc.getContractorCompanyLocOptions();
        bc.getBuildingOwnerOptions();
        bc.getPrimaryDispatcherOptions();
        bc.getPrimaryTechOptions();
        bc.getSecondaryTechOptions();
        bc.getSeconDispatcherOptions();
        bc.getSiteOptions();
        bc.save();
        bc.cancel();
        
        ApexPages.Pagereference save= bc.save(); 
        delete build; 
        save = bc.save();
        
        List<SelectOption> testOptnssdispatcher = bc.getSeconDispatcherOptions();
        List<SelectOption> testOptnsComLoc = bc.getContractorCompanyLocOptions();
        List<SelectOption> testOptnsbuildowner = bc.getBuildingOwnerOptions();
        List<SelectOption> testOptnspdispatcher = bc.getPrimaryDispatcherOptions();
        List<SelectOption> testOptnsptech = bc.getPrimaryTechOptions();
        List<SelectOption> testOptnsstech = bc.getSecondaryTechOptions();
        List<SelectOption> testOptnssiteop = bc.getSiteOptions();
    }  
}