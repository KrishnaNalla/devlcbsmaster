@isTest
public class LCBS_MyCompanyProfile_Controller_test
{
    static Account account;
    static Company_Location__c companyloc;

    static testMethod void myTest(){
     PageReference pageRef = Page.LCBS_MyCompanyProfile;
     Test.setCurrentPageReference(pageRef);
     
        LCBS_Country_State__c c = new LCBS_Country_State__c();
        c.Name='Canada';
        c.State1__c='ARG';
        c.State2__c='brazil';
        c.State3__c='MEX';
        insert c;
        account = LCBS_TestDataFactory.createAccounts(1)[0];
        companyloc = LCBS_TestDataFactory.createcompanyLocation(1)[0];
        insert account;
        
        insert companyloc;
        Test.StartTest();
        LCBS_MyCompanyProfile_Controller bc = new LCBS_MyCompanyProfile_Controller ();
        bc.insertLocation();
        delete companyloc;
       
        bc.insertLocation();
        //bc.getContractorCompanyLoc();
       // bc.newBuildingOwner();
       // bc.buidingCancel();
       // bc.buidingSave();
        bc.updateCompany();
        bc.changeCompanylocation();
        bc.cancelInsertLocation();
        bc.getCountryOption();
        bc.getStateOption();
        bc.newCompanyProfile();
        bc.InsertCompanyProfile();
        bc.changeCompanyProfile();
        delete account;
        LCBS_MyCompanyProfile_Controller b = new LCBS_MyCompanyProfile_Controller ();
        Test.StopTest();
        
        
        
    }  
 }