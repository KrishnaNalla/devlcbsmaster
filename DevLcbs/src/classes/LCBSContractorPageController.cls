public class LCBSContractorPageController  {

    public boolean showNumber { get; set; }

public boolean messageSHow {get;set;}
public boolean EmptyhCPROmessageSHow{get;set;}
public boolean isContractorPresent {get;set;}



    public PageReference setPage() {
        return null;
    }

    public String PAGE_CSS { get; set; }
    private integer counter=0;  //keeps track of the offset
    private integer list_size=10; //sets the page size or number of rows
    public  integer total_size; //used to show user the total size of the list
    public list<integer> lstTotalSize {get;set;}
    public transient List<Account> acc{get;set;}
    public transient List<Account> acc1{get;set;}
    transient set<id> accID = new set<id>();
    transient set<id> accID1 = new set<id>();
    public string email {get;set;}
    public String emailId {set;get;}  
    public string AccountId {get;set;} 
    public string contactId {get;set;} 
    public List<Partner> part {get;set;}
    public User u {get;set;}
    public Integer count {get;set;}
    public List<Account> accnt {get;set;}
    public List<Account> a {get;set;}
    public boolean ShowEmailText{get;set;}
    public string EmailEdit{get;set;}
    public string contactRemoveId{get;set;}
    public Id ContactIdd{get;set;}
    Public list<Account> accList {get;set;}
    public Contact cc {get;set;}
    public string sCPRONumber{get;set;}
    public Boolean isLcbsProgramPrincipal {get;set;}
    
    public List<Account> accqueue {get;set;}

    public LCBSContractorPageController() {



    messageSHow  = false;
         accqueue = new List<Account>();
        isLcbsProgramPrincipal  =true;
        ShowEmailText = false;  
        showNumber =false;
        system.debug('sCPRONumber+++'+sCPRONumber);    
        sCPRONumber=ApexPages.currentPage().getParameters().get('CPRONumber');
        system.debug('sCPRONumber+++'+sCPRONumber);
        lstTotalSize= new list<integer>();
        PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
        
        if(sCPRONumber!=null){
        if(sCPRONumber.startswith('03') || sCPRONumber.startswith('02')){
    messageSHow  = false;
        
        }else{
    messageSHow  = false;
        
        }
        }
      
    }
    
    public pageReference addContractor()
    {
        PageReference p = new PageReference('/LCBS_New_Contractor');
        p.setRedirect(true);
        return p;
    }
    public PageReference action() {
        return null;
    }
    public pagereference invite()
    {
    
   distributorInvite.contractor = contactId;
   distributorInvite.distributor = u.id;
  /*  Account acc = [Select Id,Date_EULA_Sent__c from Account where id=:AccountId Limit 1] ;
    
    acc.Date_EULA_Sent__c = date.today();
    
    insert acc ; */
    
    cc = [select id,name,Account.Name,Account.Email__c,Account.Phone from contact where id=:contactId ];
      //  document doc = [  select id,Name from Document where name ='InviteContractor' limit 1];
      //  string imageid = doc.id; 
       
        
       // system.debug('%%Pic'+sfdcBaseURL+'/servlet/servlet.FileDownload?file='+imageid);
      //  system.debug('emailId  : '+this.emailId);
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>();
        system.debug('Email Address chiran:'+emailId);
        sendTo.add(emailId);
        mail.setToAddresses(sendTo);
        mail.setSenderDisplayName('admin@lcbs.honeywell.com');
        mail.setTargetObjectId(cc.id);
      //  mail.setSubject('You have been invited to join Honeywell LCBS');
        EmailTemplate et = [Select Id from EmailTemplate where Name = 'Distributor Invite Contractor'];
        mail.setTemplateId(et.Id);

        //<a> http://devlcbs-devlcbs-iframe.cs17.force.com/LCBSconfirmContractorEula?id='+AccountId +'</a>';       
        //string Url = '<a href= http://devlcbs-devlcbs-iframe.cs17.force.com/LCBSconfirmContractorEula?id='+AccountId +'></a>';
        //System.URL.getSalesforceBaseUrl().toExternalForm()+'/LCBSconfirmContractorEula?id='+AccountId;
       // string ConfirmURL = system.label.LCBS_ConfirmContractor_URL;
      //  string emailMessage ='Dear '+ cc.Name +'<br/><br/>Congratulations! You have been selected to market, sell and support Honeywell\'s Light Commercial Building Solution, "LCBS Connect".<br/>The LCBS Connect solution empowers service contractors to control, remotely monitor and manage HVAC equipment. The HVAC service contractor increases business efficiency by providing timely insights into the operation of the equipment under service contracts, leading to valuable service contract retention.<br/><br/>You and your '+ cc.Account.Name +' have already met and agreed to the business terms and conditions associated with LCBS Connect. If you have question about this work, please contact your Distributor contact '+ u.Contact.name+' at '+ u.Contact.Email+',&nbsp;'+ u.Contact.Phone+'. Please visit the following link to review <a href="#">Honeywell\'s privacy statement and legal agreement</a> for using the LCBS Website to generate business. Agreeing to the website terms will allow us to issue you a user name and temporary password and give you directions on next steps.<br/><br/>we look forward to a successful partnership as we enable the Service Contractor Community to serve their customers in an efficient and productive manner.<br/>Thank you.<br/><br/><br/>'+'<table style="color:white;background-color:#085394;"><tr><td><a href='+ConfirmURL+'?id='+contactId+' style="color:white;background-color:#085394;text-decoration: none;">Get started now </a></td></tr></table>';
        
      //  mail.setHtmlBody(emailMessage);
        mails.add(mail);
        if (!Test.isRunningTest()){
        Savepoint sp = Database.setSavepoint();
            Messaging.sendEmail(mails);
           Database.rollback(sp);
             
            List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email : mails) {
             Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
             emailToSend.setToAddresses(email.getToAddresses());
             emailToSend.setPlainTextBody(email.getPlainTextBody());
             emailToSend.setHTMLBody(email.getHTMLBody());
             emailToSend.setSubject(email.getSubject());
             lstMsgsToSend.add(emailToSend);
             }
            Messaging.sendEmail(lstMsgsToSend);
        
        ApexPages.Message myMsg31 = new ApexPages.Message(ApexPages.severity.CONFIRM, 'Invited the Contractor');
        ApexPages.addMessage(myMsg31);
        

        list<Account> acc2 = new list<Account>();
        system.debug('&&&&&&&&&&&:'+accnt); 
      /*  for(account acc1: accnt)
        {
            system.debug('!!!!!!!!');  
            acc1.referred_distributorr__c = u.name;
            acc2.add(acc1);
        }  */
      //  system.debug('^^^^^^^^^^');     
     //   update acc2;
        List<Contact> c = [select id,name,Account.Name,Account.Email__c,Account.Phone from Contact where id=:contactId limit 1];
        c[0].Role__c='Owner'; 
        c[0].Contractor_Invited__c = true;
        c[0].Date_EULA_Sent__c = date.today();
        c[0].Contractor_Eula_Rejected__c = false; 
        
        update c; 
        }
        string url = system.label.LCBS_Navigation_Menu_Domain ;
        PageReference pr = new PageReference(url+'LCBSContractorPage');
        //PageReference pr = new PageReference('https://devlcbs-totalbuildings.cs17.force.com/LCBSContractorPage');
        
        return pr;     
        
    }
      public pagereference reInvite()
    {
    
    distributorInvite.contractor = contactId;
   distributorInvite.distributor = u.id;
         cc = [select id,name,Account.Name,Account.Email__c,Account.Phone from contact where id=:contactId ];
       // document doc = [  select id,Name from Document where name ='InviteContractor' limit 1];
       // string imageid = doc.id; 
       // system.debug('emailId  : '+this.emailId);
        List<Messaging.SingleEmailMessage> mails1 = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
        List<String> sendTo1 = new List<String>();
        system.debug('Email Address chiran:'+emailId);
        sendTo1.add(emailId);
        mail1.setToAddresses(sendTo1);
        mail1.setSenderDisplayName('admin@lcbs.honeywell.com');
        mail1.setTargetObjectId(cc.id);
       // mail.setSubject('You have been invited to join Honeywell LCBS');
        EmailTemplate et1 = [Select Id from EmailTemplate where Name = 'Distributor Invite Contractor'];
        mail1.setTemplateId(et1.Id);


        //<a> http://devlcbs-devlcbs-iframe.cs17.force.com/LCBSconfirmContractorEula?id='+AccountId +'</a>';       
        //string Url = '<a href= http://devlcbs-devlcbs-iframe.cs17.force.com/LCBSconfirmContractorEula?id='+AccountId +'></a>';
        //System.URL.getSalesforceBaseUrl().toExternalForm()+'/LCBSconfirmContractorEula?id='+AccountId;
       // string ConfirmURL = system.label.LCBS_ConfirmContractor_URL;
      //  string emailMessage ='Dear '+ cc.Name +'<br/><br/>Congratulations! You have been selected to market, sell and support Honeywell\'s Light Commercial Building Solution, "LCBS Connect".<br/>The LCBS Connect solution empowers service contractors to control, remotely monitor and manage HVAC equipment. The HVAC service contractor increases business efficiency by providing timely insights into the operation of the equipment under service contracts, leading to valuable service contract retention.<br/><br/>You and your '+ cc.Account.Name +' have already met and agreed to the business terms and conditions associated with LCBS Connect. If you have question about this work, please contact your Distributor contact '+ u.Contact.name+' at '+ u.Contact.Email+',&nbsp;'+ u.Contact.Phone+'. Please visit the following link to review <a href="#">Honeywell\'s privacy statement and legal agreement</a> for using the LCBS Website to generate business. Agreeing to the website terms will allow us to issue you a user name and temporary password and give you directions on next steps.<br/><br/>we look forward to a successful partnership as we enable the Service Contractor Community to serve their customers in an efficient and productive manner.<br/>Thank you.<br/><br/><br/>'+'<table style="color:white;background-color:#085394;"><tr><td><a href='+ConfirmURL+'?id='+contactId+' style="color:white;background-color:#085394;text-decoration: none;">Get started now </a></td></tr></table>';
        
       // mail.setHtmlBody(emailMessage);

        mails1.add(mail1);
        if (!Test.isRunningTest()){
        Savepoint sp = Database.setSavepoint();
            Messaging.sendEmail(mails1);
           Database.rollback(sp);
             
            List<Messaging.SingleEmailMessage> lstMsgsToSend1 = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email1 : mails1) {
             Messaging.SingleEmailMessage emailToSend1 = new Messaging.SingleEmailMessage();
             emailToSend1.setToAddresses(email1.getToAddresses());
             emailToSend1.setPlainTextBody(email1.getPlainTextBody());
             emailToSend1.setHTMLBody(email1.getHTMLBody());
             emailToSend1.setSubject(email1.getSubject());
             lstMsgsToSend1.add(emailToSend1);
             }
            Messaging.sendEmail(lstMsgsToSend1); 
        ApexPages.Message myMsg3 = new ApexPages.Message(ApexPages.severity.CONFIRM, 'Invited the Contractor');
        ApexPages.addMessage(myMsg3);
        
        

        list<Account> acc2 = new list<Account>();
        system.debug('&&&&&&&&&&&:'+accnt); 
      /*  for(account acc1: accnt)
        {
            system.debug('!!!!!!!!');  
            acc1.referred_distributorr__c = u.name;
            acc2.add(acc1);
        }  */
      //  system.debug('^^^^^^^^^^');     
     //   update acc2;
        List<Contact> c = [select id,name from Contact where id=:contactId limit 1];
        c[0].Role__c='Owner'; 
        c[0].Contractor_Invited__c = true;
        c[0].Date_EULA_Sent__c = date.today();
        c[0].Contractor_Eula_Rejected__c = false;
        update c; 
        }
        string url = system.label.LCBS_Navigation_Menu_Domain ;
        PageReference pr = new PageReference(url+'LCBSContractorPage');
        //PageReference pr = new PageReference('https://devlcbs-totalbuildings.cs17.force.com/LCBSContractorPage');
        
        return pr;     
        
    }
    public void removeAccess()
    {
     if (!Test.isRunningTest()){
     User u = [select id,name,IsActive from User where ContactId=:contactRemoveId limit 1];
     u.IsActive = false;
     update u;
     }
    }
    
    public pageReference updateContactRemoval()
    {
     Contact c = [select id,Name,Contractor_Invited__c,Date_EULA_Sent__c from Contact where id=:contactRemoveId limit 1];
     c.Date_EULA_Sent__c =date.today();
     c.Contractor_Invited__c =false;
     c.Contractor_Eula_Rejected__c =false;
     c.Contractor_Eula_Accepted__c = false;
     update c; 
     string url = system.label.LCBS_Navigation_Menu_Domain;
     PageReference p1 = new PageReference(url+'LCBSContractorPage');
     return p1;
    }
    public void requestCPRO()
    {
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>();
        sendTo.add(email);
        mail.setToAddresses(sendTo);
        mail.setSenderDisplayName('Contractor CPRO Number');
        mail.setSubject('Request for CPRO number');
        string emailMessage ='Please follow the below URL to request for the CPRO Number <br/> <br/> <a> www.contractorpro.com </a>';
        mail.setHtmlBody(emailMessage);
        mails.add(mail);
        if (!Test.isRunningTest()){
        Messaging.sendEmail(mails);
        }
        ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.severity.CONFIRM, 'Requested the CPRO Number');
        ApexPages.addMessage(myMsg2);
    }
    
    public void ShowEmailTextBoxInfo(){
        ShowEmailText = true;
    }
    
    public void SaveEmailText(){
        ShowEmailText = false;      
        ContactIdd=Apexpages.currentPage().getParameters().get('ContactIdd');
        system.debug(ContactIdd);  
        contact c = new contact();
        c.Id = ContactIdd;
        c.Email = 'test@gmail.com';
        try{
        update c;
        } catch(DMLException e){}
    }
    
    List<categoryWrapper> categories {get;set;}

    // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con {
        get {
        
            if(con == null) {
            
                RecordType RT = [select id,Name from RecordType where Name = 'Contractors'];
                acc = [select id,Name from Account where recordtypeid =: RT.Id];
            
                for(Account a : acc){
                    accId.add(a.id);
                    
                } 
               
                system.debug('sCPRONumber++++'+sCPRONumber);        
                u = [Select Id,name,Contact.AccountId,Contact.FirstName,Contact.Lastname,Contact.Name,Contact.Phone,Contact.Email from User where ID =:UserInfo.getUserId()];
                sortMethod();
               /*
                if(sCPRONumber==null){
                   accqueue = [Select Id, Name,referred_distributorr__c,(Select FirstName,LastName,phone,email,role__c,ContractorProContact__c,
Contractor_Invited__c,Contractor_EULA_Accepted__c,Contractor_Eula_Rejected__c,Date_EULA_Sent__c,Date_EULA_Accepted__c,
Date_EULA_Rejected__c,isContainFedId__c,LCBS_Program_Principal__c  from Contacts  where ContractorProContact__c = true AND LCBS_Program_Principal__c = true),EnvironmentalContractorProNumber__c,Address__c,
City__c,Phone,Email__c FROM Account where ID IN (Select AccountToId from Partner where AccountFromId =:u.Contact.AccountId) AND 
EnvironmentalContractorProNumber__c!=null AND 
ID IN (Select AccountId from contact where ContractorProContact__c = true) Order By Name ]; 
                   con = new ApexPages.StandardSetController(accqueue);
                
                    total_size = con.getResultSize();
                    system.debug('%%result size'+total_size);
                    
                
                    total_size = total_size/10;
                    system.debug('total_size ++++'+total_size);
                    integer i;    
                    for (i=1; i<=total_size; i++) {
                        lstTotalSize.add(i);
                    }
                    
                    if(lstTotalSize.size()>10){
                        showNumber=true;
                    }
                    system.debug(lstTotalSize.size()+'lsttotalsize+++++');
                }else{
                    system.debug('sCPRONumber++++'+sCPRONumber);
                    
                    con = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id, Name,referred_distributorr__c,(Select FirstName,LastName,phone,email,role__c,ContractorProContact__c,Contractor_Invited__c,Contractor_EULA_Accepted__c,Contractor_Eula_Rejected__c,Date_EULA_Sent__c,isContainFedId__c,LCBS_Program_Principal__c from Contacts  where ContractorProContact__c = true AND LCBS_Program_Principal__c = true),EnvironmentalContractorProNumber__c,Address__c,City__c,Phone,Email__c FROM Account where ID IN (Select AccountToId from Partner where AccountFromId =:u.Contact.AccountId) AND EnvironmentalContractorProNumber__c=:sCPRONumber AND 
                             ID IN (Select AccountId from contact where ContractorProContact__c = true ) Order By Name ]));
                    system.debug('con++++'+con);
                   
                    total_size = con.getResultSize();
                    
                    integer i;
                    for (i=1; i<=total_size; i++) {
                        lstTotalSize.add(i);
                    }
                    system.debug(lstTotalSize.size()+'lsttotalsize+++++');
                    system.debug('total_size ++++'+total_size);
                }
                */
                // sets the number of records in each page set
                con.setPageSize(10);
            }
             
            return con;
        } 
        set;
    }

    // returns a list of wrapper objects for the sObjects in the current page set
    public List<categoryWrapper> getCategories() {
        categories = new List<categoryWrapper>();
        for (Account category : (List<Account>)con.getRecords())
          
              
            
           categories.add(new CategoryWrapper(category)); 
            
            
            
        return categories;
    }
    Public string selectedField {get;set;}
    
    Public void sortMethod(){
    system.debug('Test0000000'+selectedField);
    system.debug('CproNumber000***'+sCPRONumber);
    
    if(sCPRONumber== '' || sCPRONumber== Null){
     if(selectedField == 'NameAsc'){
                   accqueue = [Select Id, Name,referred_distributorr__c,(Select FirstName,LastName,phone,email,role__c,ContractorProContact__c,
Contractor_Invited__c,Contractor_EULA_Accepted__c,Contractor_Eula_Rejected__c,Date_EULA_Sent__c,Date_EULA_Accepted__c,
Date_EULA_Rejected__c,isContainFedId__c,LCBS_Program_Principal__c  from Contacts  where ContractorProContact__c = true AND LCBS_Program_Principal__c = true order by LastName Asc),EnvironmentalContractorProNumber__c,Address__c,
City__c,Phone,Email__c FROM Account where ID IN (Select AccountToId from Partner where AccountFromId =:u.Contact.AccountId) AND 
EnvironmentalContractorProNumber__c!=null AND 
ID IN (Select AccountId from contact where ContractorProContact__c = true) ];
con = new ApexPages.StandardSetController(accqueue);
system.debug('NameAsc**************');
} 
else if(selectedField == 'ContractorAsc'){
                   accqueue = [Select Id, Name,referred_distributorr__c,(Select FirstName,LastName,phone,email,role__c,ContractorProContact__c,
Contractor_Invited__c,Contractor_EULA_Accepted__c,Contractor_Eula_Rejected__c,Date_EULA_Sent__c,Date_EULA_Accepted__c,
Date_EULA_Rejected__c,isContainFedId__c,LCBS_Program_Principal__c  from Contacts  where ContractorProContact__c = true AND LCBS_Program_Principal__c = true),EnvironmentalContractorProNumber__c,Address__c,
City__c,Phone,Email__c FROM Account where ID IN (Select AccountToId from Partner where AccountFromId =:u.Contact.AccountId) AND 
EnvironmentalContractorProNumber__c!=null AND 
ID IN (Select AccountId from contact where ContractorProContact__c = true) Order By Name Asc];
con = new ApexPages.StandardSetController(accqueue);
system.debug('Chiru**************');
} 
else if(selectedField == 'EmailAsc'){
                   accqueue = [Select Id, Name,referred_distributorr__c,(Select FirstName,LastName,phone,email,role__c,ContractorProContact__c,
Contractor_Invited__c,Contractor_EULA_Accepted__c,Contractor_Eula_Rejected__c,Date_EULA_Sent__c,Date_EULA_Accepted__c,
Date_EULA_Rejected__c,isContainFedId__c,LCBS_Program_Principal__c  from Contacts where ContractorProContact__c = true AND LCBS_Program_Principal__c = true Order By Email Asc),EnvironmentalContractorProNumber__c,Address__c,
City__c,Phone,Email__c FROM Account where ID IN (Select AccountToId from Partner where AccountFromId =:u.Contact.AccountId) AND 
EnvironmentalContractorProNumber__c!=null AND 
ID IN (Select AccountId from contact where ContractorProContact__c = true) ];
con = new ApexPages.StandardSetController(accqueue);
system.debug('Chiru%%%%%%%%%');
} 
else if(selectedField == 'CPROAsc'){
                   accqueue = [Select Id, Name,referred_distributorr__c,(Select FirstName,LastName,phone,email,role__c,ContractorProContact__c,
Contractor_Invited__c,Contractor_EULA_Accepted__c,Contractor_Eula_Rejected__c,Date_EULA_Sent__c,Date_EULA_Accepted__c,
Date_EULA_Rejected__c,isContainFedId__c,LCBS_Program_Principal__c  from Contacts  where ContractorProContact__c = true AND LCBS_Program_Principal__c = true),EnvironmentalContractorProNumber__c,Address__c,
City__c,Phone,Email__c FROM Account where ID IN (Select AccountToId from Partner where AccountFromId =:u.Contact.AccountId) AND 
EnvironmentalContractorProNumber__c!=null AND 
ID IN (Select AccountId from contact where ContractorProContact__c = true) Order By EnvironmentalContractorProNumber__c asc];
con = new ApexPages.StandardSetController(accqueue);
system.debug('Chiru$$$$$$$$$$$');
} 
else if(selectedField == 'InvAsc'){
                   accqueue = [Select Id, Name,referred_distributorr__c,(Select FirstName,LastName,phone,email,role__c,ContractorProContact__c,
Contractor_Invited__c,Contractor_EULA_Accepted__c,Contractor_Eula_Rejected__c,Date_EULA_Sent__c,Date_EULA_Accepted__c,
Date_EULA_Rejected__c,isContainFedId__c,LCBS_Program_Principal__c  from Contacts  where ContractorProContact__c = true AND LCBS_Program_Principal__c = true),EnvironmentalContractorProNumber__c,Address__c,
City__c,Phone,Email__c FROM Account where ID IN (Select AccountToId from Partner where AccountFromId =:u.Contact.AccountId) AND 
EnvironmentalContractorProNumber__c!=null AND 
ID IN (Select AccountId from contact where ContractorProContact__c = true) Order By EnvironmentalContractorProNumber__c desc];
con = new ApexPages.StandardSetController(accqueue);
system.debug('Chiru^^^^^^^^^');
} 
else {
accqueue = [Select Id, Name,referred_distributorr__c,(Select FirstName,LastName,phone,email,role__c,ContractorProContact__c,
Contractor_Invited__c,Contractor_EULA_Accepted__c,Contractor_Eula_Rejected__c,Date_EULA_Sent__c,Date_EULA_Accepted__c,
Date_EULA_Rejected__c,isContainFedId__c,LCBS_Program_Principal__c  from Contacts  where ContractorProContact__c = true AND LCBS_Program_Principal__c = true),EnvironmentalContractorProNumber__c,Address__c,
City__c,Phone,Email__c FROM Account where ID IN (Select AccountToId from Partner where AccountFromId =:u.Contact.AccountId) AND 
EnvironmentalContractorProNumber__c!=null AND 
ID IN (Select AccountId from contact where ContractorProContact__c = true) Order By Name desc ];
con = new ApexPages.StandardSetController(accqueue);
system.debug('Elseeeeeee^^^^^^^^^');
}

                  // con = new ApexPages.StandardSetController(accqueue);
                
                    total_size = con.getResultSize();
                    system.debug('%%result size'+total_size);
                    
                
                    total_size = total_size/10;
                    system.debug('total_size ++++'+total_size);
                    integer i;    
                    for (i=1; i<=total_size; i++) {
                        lstTotalSize.add(i);
                    }
                    
                    if(lstTotalSize.size()>10){
                        showNumber=true;
                    }
                    system.debug(lstTotalSize.size()+'lsttotalsize+++++');
                }else{
                    system.debug('sCPRONumber++++'+sCPRONumber);
                    
                    con = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id, Name,referred_distributorr__c,(Select FirstName,LastName,phone,email,role__c,ContractorProContact__c,Contractor_Invited__c,Contractor_EULA_Accepted__c,Contractor_Eula_Rejected__c,Date_EULA_Sent__c,isContainFedId__c,LCBS_Program_Principal__c from Contacts  where ContractorProContact__c = true AND LCBS_Program_Principal__c = true),EnvironmentalContractorProNumber__c,Address__c,City__c,Phone,Email__c FROM Account where ID IN (Select AccountToId from Partner where AccountFromId =:u.Contact.AccountId) AND EnvironmentalContractorProNumber__c=:sCPRONumber AND 
                             ID IN (Select AccountId from contact where ContractorProContact__c = true ) Order By Name ]));
                    system.debug('con++++'+con);
                   
                    total_size = con.getResultSize();
                    
                    integer i;
                    for (i=1; i<=total_size; i++) {
                        lstTotalSize.add(i);
                    }
                    system.debug(lstTotalSize.size()+'lsttotalsize+++++');
                    system.debug('total_size ++++'+total_size);
                }
       
    
    }
      
    Public list<SelectOption> getSortOptions(){
        list<SelectOption> sortValues =  new list<SelectOption>();
        sortValues.add(new SelectOption('NameAsc','Name'));
        sortValues.add(new SelectOption('ContractorAsc','Contractor Company'));
        sortValues.add(new SelectOption('EmailAsc','Email Address'));
        sortValues.add(new SelectOption('CPROAsc','CPRO Number'));
        sortValues.add(new SelectOption('InvAsc','Invitation Status'));
    return sortValues;
    }
     
    // displays the selected items
     public PageReference process() {
         /*for (CategoryWrapper cw : categories) {
             if (cw.checked)
                 ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,cw.cat.name));
         }*/
         return null;
     }

    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }

    // returns the first page of records
     public void first() {
         con.first();
     }

     // returns the last page of records
     public void last() {
         con.last();
     }

     // returns the previous page of records
     public void previous() {
         con.previous();
     }

     // returns the next page of records
     public void next() {
         con.next();
     }

     // returns the PageReference of the original page, if known, or the home page.
     public void cancel() {
         con.cancel();
     }
     
    public class CategoryWrapper {

        //public Boolean checked{ get; set; }
        //public list<Contact> cat { get; set;}
        public Account Acc{get;set;}

        public CategoryWrapper(){
            Acc = new Account();
            //checked = false;
        }

        public CategoryWrapper(Account a){
            Acc= a;
            //checked = false;
        }

    }
    
  /*  public pagereference setCPRONumber(){
        system.debug('sCPRONumber++++'+sCPRONumber);
        //first();
        string s = sCPRONumber.trim();
        
        
        PageReference pageRef = new PageReference('/LCBSContractorPage');
        
        if(s!=null && s!=''){
            pageRef.getParameters().put('CPRONumber', sCPRONumber);
        }
        else{
        ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.severity.ERROR, 'CPRO Number not found');
        ApexPages.addMessage(myMsg1);
        }
               
            return pageRef;
        //return null;
    }
    */
        public pagereference setCPRONumber(){
        EmptyhCPROmessageSHow=false;
        messageSHow=false;
        system.debug('sCPRONumber++++'+sCPRONumber);
        //first();
        string s = sCPRONumber.trim();     
       // system.debug('#######Chiran#####:'+con.getResultSize());
        PageReference pageRef = new PageReference('/LCBSContractorPage');
        RecordType RT = [select id,Name from RecordType where Name = 'Contractors'];
        u = [Select Id,name,Contact.AccountId,Contact.FirstName,Contact.Lastname,Contact.Name,Contact.Email,Contact.Phone from User where ID =:UserInfo.getUserId()];
        accList = [select id,Name,EnvironmentalContractorProNumber__c from Account where recordtypeid =: RT.Id and EnvironmentalContractorProNumber__c =: s and id  IN (Select AccountToId from Partner where AccountFromId =:u.Contact.AccountId) ];
       system.debug('@@@@@@@Chiran@@@@@'+s);
                              
        if (accList.isEmpty()){ 
         if(String.isNotBlank(sCPRONumber)) {  
        system.debug('sCPRONumber+****+++'+sCPRONumber);    
        messageSHow = true;
        return null;  
        }         
        }  
        
        if(s!=null && s!=''){
        pageRef.getParameters().put('CPRONumber', sCPRONumber);
         return pageRef;
        }
        else{
        EmptyhCPROmessageSHow = true;
        return null;
        }                           
        return null;
    }

    
    public pagereference setPageNumber(){
        integer myParam = integer.valueof(apexpages.currentpage().getparameters().get('myParam'));
        system.debug('myparam+++'+myParam);
        con.setPageNumber(myParam);
        return null;
    }
    
    public pagereference EditEmailValue(){
        EmailEdit=Apexpages.currentPage().getParameters().get('eValue');
        ShowEmailText = true;
        system.debug('EmailEdit++++'+EmailEdit);
        return null;
    }
    
    public void sendusMail()
    {
     List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>();
        sendTo.add('hma@malinator.com');
        sendTo.add('salesrep@malinator.com');
        mail.setToAddresses(sendTo);
        mail.setSenderDisplayName('Contractor CPRO Number');
        mail.setSubject('LCBS Portal');
        string emailMessage ='Please redirect to  www.contractorpro.com for further information.';
        mail.setHtmlBody(emailMessage);
        mails.add(mail);
        if (!Test.isRunningTest()){
        Messaging.sendEmail(mails);
        }
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.CONFIRM, 'Email has been sent to admin');
        ApexPages.addMessage(myMsg);
    }
    
    public void nothing(){}
     public void emailEditSave()
    {
     List<Contact> con = new List<Contact>();
     for(CategoryWrapper c : categories)
     {
      con.addAll(c.acc.Contacts);
     }
     update con; 
     //isEmailEdit =false;
    }
}