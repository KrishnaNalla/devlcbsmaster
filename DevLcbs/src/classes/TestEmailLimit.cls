public class TestEmailLimit{
    private final Contact con;
    public TestEmailLimit(ApexPages.StandardController controller){
        this.con=(Contact)controller.getRecord();
    }
    public void SendEmail(){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId('005g0000002Lvqe');
        mail.setsaveAsActivity(false);
        //mail.setTemplateId('00Xg0000000J1jp');
        mail.setHtmlBody('Email Body');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}