public class AddBuildingCallout
{
    //public string BuildingName{get;set;}
    //public string BuildingId{get;set;}

 public static string geBuildings(string BuildingName,string BuildingId)
 {
    azureactiveDirectoryHelper activeDirectoryHelper = new azureactiveDirectoryHelper();
    String authorizationHeader = activeDirectoryHelper.getLcbsWebApiAccessToken();
    HttpRequest req = new HttpRequest();
    HttpResponse res = new HttpResponse();
    Http http = new Http();
    //Building__c B = new Building__c();
    //if(BuildingName==null && BuildingName==''){
        //BuildingName= 'Harriet Center';
    //}
    //if(BuildingId==null && BuildingId==''){
        //BuildingId= 'a02g000000CAHP2';
    //}
    //if(OwnerId==null && OwnerId==''){
        //OwnerId='005g0000001uhrU';
    //}
    
    JSONGenerator gen = JSON.createGenerator(true);
    gen.writeStartObject();        
    gen.writeObjectField('BuildingName', BuildingName); 
    gen.writeObjectField('BuildingId', BuildingId); 
    gen.writeEndObject();
    String jsonString = gen.getAsString();

    system.debug('jsonString +++'+jsonString );
    //string addBuildingJsonRequest = JSON.serialize(B);
    req.setHeader('Authorization', authorizationHeader); 
    req.setHeader('Content-Type', 'application/json');
    List<LCBS_API_Details__c> CalloutDetails= LCBS_API_Details__c.getall().values();
    
    if (CalloutDetails[0].End_Pt_URL__c!=null && CalloutDetails[0].End_Pt_URL__c!='') {
      req.setEndpoint(CalloutDetails[0].End_Pt_URL__c);
    }
    
    system.debug('req.setEndPoint+++'+CalloutDetails[0].End_Pt_URL__c);
    req.setMethod('POST');
    
    //these parts of the POST you may want to customize
    req.setCompressed(false);
    req.setBody(jsonString);
    //Http http = new Http();
    res= http.send(req);
    String buildingRes = res.getBody();
    //JsonDeserialize buildings = new JsonDeserialize();
    //system.debug('Response value++++'+buildingRes);
    //buildings = (JsonDeserialize) JSON.deserialize(buildingRes,JsonDeserialize.class);
    //System.debug(buildings);
    //if(buildings.status=='Accepted'){
    //    return buildings.Status;
    //}else{
    //    return null;
    //}
    
    //get the list of header names (keys)
    //string[] headerkeys = res.getHeaderKeys();
    
    //create an object to store your header key-value pairs
    /*Map<string, string> headers = new map<string, string>();
    system.debug('headerkeys++'+res.getStatusCode());
    //iterate through they keys, and populate your map
    for(string s : headerkeys){
       if(s!=null && s!=''){
       headers.put(s,res.getHeader(s));
       system.debug('header: ' + s + ' value: ' + res.getHeader(s));
       }
    }*/
    system.debug('Status of Request++'+res.getStatusCode());
    return null;
    
 }   
 
 public class Jsonserialize
{
    string BuildingName;
    String BuildingId;
        
}
 }