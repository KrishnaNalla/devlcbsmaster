/************************************
Name         : LCBS_InviteDistributorController
Created By   : Nathan Lourdusamy
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  :
***********************************/

public class LCBS_InviteDistributorController{

    public Id AcctId{get; set;}
    public String AcctName{get; set;}
    // public String AcctExecutive{get; set;}
    public String AccountExeName{get; set;}
    public String AccountExeEmail{get; set;}
    public String AccountExePhone{get; set;}
    public String siteurl{get; set;}
    public contact cc{get; set;}
    public String PAGE_CSS{get; set;}
    public user u{get; set;}
    // public Account c{get; set;}
    public document sign{get; set;}
    public document logo{get; set;}
    String Env;
    // public string executive_name{get; set;}
    // public string executive_email{get; set;}
    // public string executive_phone{get; set;}
    public string url{get; set;}
    public string orgid{get; set;}
    public string sfdcBaseURL{get; set;}
    public Account ac{get; set;}
    public String d {get;set;}
    public LCBS_InviteDistributorController(){
        //AcctId = ApexPages.CurrentPage().getparameters().get('id');
        ac= new Account();
        d= DateTime.now().format('MMMMM dd, yyyy');
        if(AcctId != NULL){
            ac = [select id, name, Account_Exec_Name__c, Account_Exec_Email__c, Account_Exec_Phone__c from Account where id=: AcctId];
            // Account_Executive_Info__c
            //ApexPages.CurrentPage().getparameters().get('id')];
        }
        
        // system.debug('##Naga'+ac);
        // Site ste = [SELECT Id, Name, Description FROM Site WHERE Name='LCBS_EULADistributor'];
        // siteurl = ste.description;
        sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm();
        orgid= UserInfo.getOrganizationId();
        sign = [select id,name from document where name='Honeywell vicepresident sign'];
        logo = [select id,name from document where name='LCBS_Email_Logo'];
        //cc = [select id,name,firstname,lastname,Account.Name,Account.Email__c,account.Phone from contact where id=:contractor];
        //system.debug('Invited Contacts'+cc);
        //u = [select id,name,federationIdentifier,contactid,Contact.AccountId,Contact.FirstName,Contact.Lastname,Contact.Name,Contact.Phone,Contact.Email,contact.account.email__c,contact.account.phone,contact.account.name from user where id=:distributor];
        //system.debug('Distributor Contact'+u);
        //c = [select id,name,(select id,firstname,lastname,email,phone from contacts) from Account where id=:system.label.Honeywell_Account_Executive];
        //system.debug('%%'+c.contacts[0].firstname);
        
        if(ac.Account_Exec_Name__c != NULL){
            AccountExeName = ac.Account_Exec_Name__c;
        }
        else{
            AccountExeName = '';
        }
        
        if(ac.Account_Exec_Email__c != NULL){
            AccountExeEmail = ac.Account_Exec_Email__c;
        }
        else{
            AccountExeEmail = '';
        }
        
        if(ac.Account_Exec_Phone__c != NULL){
            AccountExePhone = ac.Account_Exec_Phone__c;
        }
        else{
            AccountExePhone = '';
        }
        
        /* if(ac.Account_Executive_Info__c != NULL){
            String[] exec = ac.Account_Executive_Info__c .split('\\|');
            executive_name = ac.Account_Exec_Name__c;
            executive_email = ac.Account_Exec_Email__c;
            executive_phone = ac.Account_Exec_Phone__c;
        }
        
        else{
            executive_name = '';
            executive_email = '';
            executive_phone = '';
        } */
        
        // PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
    }
    
    /* public Account Acctinfo{
        get{
            Acctinfo = [Select Id,Email__c from Account where id =: AcctId];
            return Acctinfo ;
        }
        set;
    } */
    
}