public class ActiveDirectoryToken
{
    public String access_token;
    public Integer expires_in;
    public Integer expires_on;
    public Integer not_before;
    public String resource;
    public String token_type;
    public ActiveDirectoryToken()
    {
    }
}