<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-UserProfile</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-File</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Azure</tab>
    <tab>Azure1</tab>
    <tab>Building__c</tab>
    <tab>Building_Owner__c</tab>
    <tab>Site__c</tab>
    <tab>Contractor_Branch_Location__c</tab>
    <tab>Company_Location__c</tab>
    <tab>Building_Pre_approval_Tracking__c</tab>
    <tab>Community</tab>
    <tab>Community_Index</tab>
    <tab>Community_Distributor</tab>
    <tab>LCBS_RequestResponse_Log__c</tab>
</CustomApplication>
