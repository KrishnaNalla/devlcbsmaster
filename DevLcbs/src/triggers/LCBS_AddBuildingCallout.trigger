trigger LCBS_AddBuildingCallout on Building__c (after insert, after update) {
Building__c build = trigger.new[0];

//Building_c owner = [select id,name,Building_Owner__r.id,Building_Owner__r.Name from Building__c where id=:build.id];
 if(build.Building_Owner__c!=NULL && build.Owner_Approved__c && (build.Owner_Approved__c != trigger.oldmap.get(build.Id).Owner_Approved__c || (build.Technician__c!= trigger.oldmap.get(build.Id).Technician__c || build.Dispatcher__c!= trigger.oldmap.get(build.Id).Dispatcher__c ||  build.Alternate_Technician__c!= trigger.oldmap.get(build.Id).Alternate_Technician__c ||  build.Alternate_Dispatcher__c != trigger.oldmap.get(build.Id).Alternate_Dispatcher__c )))
 {
  Building_Owner__c owner = [select id,name from Building_Owner__c where id=:build.Building_Owner__c];
  system.debug(build.Name);
  system.debug(build.Id);
  system.debug(build.Building_Owner__c);
  //system.debug(build.Building_Owner__r.Name);
  LCBSBuildingCallout.getBuildings(build.Name,build.Id, owner.id, owner.Name);
  
 } 
}